#include "mod_sahtrace.odl";
#include "mod_apparmor.odl";
#include "global_amxb_timeouts.odl";

%config {
    name = "wan-manager";
    import-dbg = false;

    vendor_prefix = {
        "dns" = "X_PRPL-COM_"
    };

    // SAHTRACE
    sahtrace = {
        type = "syslog",
        level = "${default_log_level}"
    };

    trace-zones = {
        "bridge-mode" = "${default_trace_zone_level}",
        "common-layer" = "${default_trace_zone_level}",
        "wan-man" = "${default_trace_zone_level}",
        "uci-ctrl" = "${default_trace_zone_level}",
        "dhcpc-ctrl" = "${default_trace_zone_level}",
        "ppp-ctrl" = "${default_trace_zone_level}",
        "as-ctrl" = "${default_trace_zone_level}",
        "com-ctrl" = "${default_trace_zone_level}",
        "eth-ctrl" = "${default_trace_zone_level}",
        "netmod-ctrl" = "${default_trace_zone_level}",
        "dslite-ctrl" = "${default_trace_zone_level}",
        "static-ctrl" = "${default_trace_zone_level}",
        "mod-autosensing" = "${default_trace_zone_level}"
    };

    // main files
    wan_manager_definition_file = "${name}_definition.odl";
    wan_mode_definition_file = "${name}_WAN_definition.odl";
    wan_mode_intf_definition_file = "${name}_WAN_Intf_definition.odl";

    // Use ODL persistent storage
    storage-path = "${rw_data_path}/${name}";
    odl = {
        dm-load = true,
        dm-save = true,
        dm-save-on-changed = true,
        dm-save-delay = 1000,
        dm-defaults = "defaults.d/",
        directory = "${storage-path}/odl"
    };

    external-mod-dir = "/usr/lib/amx/modules";

    pcm_svc_config = {
        "Objects" = "WANManager"
    };

    // default value(s) for Device.IP.Interface reference
    %global ip_intf_lo = "Device.IP.Interface.1.";
    %global ip_intf_wan = "Device.IP.Interface.2.";
    %global ip_intf_lan = "Device.IP.Interface.3.";
    %global ip_intf_guest = "Device.IP.Interface.4.";
    %global ip_intf_lcm = "Device.IP.Interface.5.";
    %global ip_intf_wan6 = "Device.IP.Interface.6.";
    %global ip_intf_dslite-entry = "Device.IP.Interface.7.";
    %global ip_intf_dslite-exit = "Device.IP.Interface.8.";
    %global ip_intf_voip = "Device.IP.Interface.9.";
    %global ip_intf_iptv = "Device.IP.Interface.10.";
    %global ip_intf_mgmt = "Device.IP.Interface.11.";
}
#include "global_ip_interfaces.odl";

import "${name}.so" as "${name}";
import "mod-dmext.so";
#include "${name}_caps.odl";

include "${wan_manager_definition_file}";
include "${wan_mode_definition_file}";
include "${wan_mode_intf_definition_file}";

requires "IP.";
requires "DHCPv4Client.Client.";
requires "DHCPv6Client.Client.";
requires "Routing.";
requires "DNS.";
requires "Logical.";

%define {
    entry-point "${name}".wan_manager_main;
}

#include "mod_pcm_svc.odl";

%populate {
    on event "app:start" call app_start;
}