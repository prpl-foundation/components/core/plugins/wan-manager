%define{
    %persistent object 'WANManager' {
        /**
        * Only modules that are added to this comma separated list can be configured
        * in the Controller parameters.
        * The name used should be the name of the so file without the extension.
        *
        * @version 1.0
        */
        %read-only %protected csv_string SupportedControllers = "mod-autosensing";

        /**
        * Configures the module that should be used for autosensing.
        * Can only be one of the supported controllers configured in SupportedControllers
        *
        * @version 1.0
        */
        %persistent %protected string Controller = "mod-autosensing" {
            on action validate call check_is_empty_or_in "WANManager.SupportedControllers";
        }

        /**
        * When this parameter is set to true, the wan mode will be applied at boot.
        * After applying the config, this parameter will be set to false.
        *
        * @version 1.0
        */
        %persistent %protected bool ApplyAtNextBoot = true {
            userflags %upc;
        }

        /**
         * Set WAN mode
         * @param WANMode - value to which to change WANMode
         * @param Autosensing - Change requested by WANAutosensing
         * @return - true on success
         * @version 1.0
         */
        bool setWANMode(%in %mandatory string WANMode, %in string Autosensing);

        /**
         * Set IPv4 mode for the given interface in the current WANMode
         * @param IPv4 mode - Value to which to change the IPv4Mode
         * @param InterfaceAlias - Alias of the wan mode interface to act on
         * @return - true on success
         * @version 1.0
         */
        bool setIPv4Mode(%in %mandatory string IPv4Mode, %in %mandatory string InterfaceAlias);

        /**
         * Set IPv6 mode for the given interface in the current WANMode
         * @param IPv6 mode - Value to which to change the IPv6Mode
         * @param InterfaceAlias - Alias of the wan mode interface to act on
         * @return - true on success
         * @version 1.0
         */
        bool setIPv6Mode(%in %mandatory string IPv6Mode, %in %mandatory string InterfaceAlias);

        /**
         * On a reset the current WANMode MUST be teared down and established again.
         * @version V1.0
         * type command
         */
        void Reset();

        /**
         * Get status of current WAN mode. Use by WANAutosensing
         * @return - true when WANMode status is Enabled and configured interface obtain IP address
         * @version 1.0
         */
        bool getCurrentWANModeStatus();

        /**
         *
         * Get WAN mode
         * @return - complete map with the whole configuration
         * @version 1.0
         */
        string getWANMode();

        /**
         * Mode to which WAN interface is configured to
         * @version 1.0
         */
        %persistent string WANMode {
            on action validate call check_maximum_length 64;
            default "";
            userflags %usersetting;
        }

        /**
         * WANMode sensing operational mode.
         * * OperationMode = Manual: The WANMode will be set fixed.
         * * OperationMode = Automatic: The WANMode will be selected based on a WAN Autosensing mechanisme.
         * @version 1.0
         */
        %persistent string OperationMode {
            on action validate call check_enum ["Manual", "Automatic"];
            default "Manual";
        }

        /**
         * WANMode sensing Policy: Defines how the autosensing mechanism should work. Different behavior can be applied
         * * Policy = AtBoot: WAN Sensing will be started at boot, once a WANMode is detected, it will be set 'fixed'
         * * Policy = Continuous: The WANMode selection procedure will be continuous looping over all applicable WANModes
         *   until we found a working WANMode.
         * @version 1.0
         */
         %persistent string SensingPolicy {
            on action validate call check_enum ["AtBoot", "Continuous"];
            default "AtBoot";
         }

        /**
         * Defines the maximum time to wait to decide if a WANMode is active or not.
         * Only one timeout is used for each WANMode.
         * @version 1.0
         */
         %persistent uint32 SensingTimeout {
            default 8;
         }
    }
}

%populate {
    on event "dm:object-changed" call set_wan_mode
        filter 'path == "WANManager." && contains("parameters.WANMode")';
    on event "dm:object-changed" call update_autosensing
        filter 'path == "WANManager." && contains("parameters.OperationMode")';
    on event "dm:object-changed" call update_sensing_policy
        filter 'path == "WANManager." && contains("parameters.SensingPolicy")';

}
