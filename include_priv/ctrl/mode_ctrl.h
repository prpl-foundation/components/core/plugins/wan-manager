/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__MODE_CTRL_H__)
#define __MODE_CTRL_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>

typedef enum {
    IP_None       = 0x000000,
    IPv4_DHCP     = 0x000001,
    IPv4_PPP      = 0x000002,
    IPv4_STATIC   = 0x000004,
    IPv4_DSLITE   = 0x000008,
    IPv4_LINK     = 0x000010,
    IPv6_DHCP     = 0x000100,
    IPv6_PPP      = 0x000200,
    IPv6_STATIC   = 0x000400,
    IPv6_LINK     = 0x000800,
    MASK_DHCP     = 0x000101,
    MASK_PPP      = 0x000202,
    MASK_STATIC   = 0x000404,
    MASK_LINK     = 0x000810,
    TYPE_VLAN     = 0x010000,
    TYPE_UNTAGGED = 0x020000,
    TYPE_ATM      = 0x040000,
    MASK_IPv4     = 0x0000FF,
    MASK_IPv6     = 0x00FF00,
    MASK_TYPE     = 0xFF0000
} mode_ctrl_t;

typedef amxd_status_t (* ctrl_fn)(mode_ctrl_t mode, amxc_var_t* const, bool enable);

amxd_status_t mode_ctrl_action(mode_ctrl_t mode, amxc_var_t* const parameters, bool enable);

#ifdef __cplusplus
}
#endif

#endif // __MODE_CTRL_H__
